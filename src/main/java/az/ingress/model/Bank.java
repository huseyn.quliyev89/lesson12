package az.ingress.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;


//@Getter
//@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
//@NamedEntityGraph(name = "Bank.branches",
//        attributeNodes = @NamedAttributeNode("branches")
//)
public class Bank {
    @Id
    String bankCode;
    String bankName;
    String bankSwiftCode;
    LocalDate bankOpeningDate;

    @OneToMany(mappedBy = "bank", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnoreProperties("bank")
    List<Branch> branches;
}
