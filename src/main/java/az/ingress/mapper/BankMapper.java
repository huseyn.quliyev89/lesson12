package az.ingress.mapper;

import az.ingress.dto.BankDto;
import az.ingress.dto.BranchDto;
import az.ingress.model.Bank;
import az.ingress.model.Branch;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BankMapper {
    BankDto mapToBankDto(Bank bank);
    Bank mapToBank(BankDto bankDto);

    List<BankDto> mapToBankDtoList(List<Bank> bankList);

    List<Bank> mapToBankList(List<BankDto> bankDtoList);
}
