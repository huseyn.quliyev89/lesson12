package az.ingress.repository;

import az.ingress.dto.TotalBranchByBankDto;
import az.ingress.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch,String> {
 //   @Query(value = "select b2.bankName as bankName,count(b1.bank.bankCode) as totalBranch FROM Branch b1 right join Bank b2 on b2.bankCode=b1.bank.bankCode group by b2.bankName")
    @Query(nativeQuery = true, value = "select b2.bank_name AS bankName, count(b1.bank_code) AS totalBranch FROM Branch b1 " +
            "right join Bank b2 on b2.bank_code=b1.bank_code group by b2.bank_name")
    List<TotalBranchByBankDto> totalBranchByBank();

}

