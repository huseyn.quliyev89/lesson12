package az.ingress.repository;

import az.ingress.model.Bank;
import jakarta.persistence.EntityManager;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BankRepository extends JpaRepository<Bank,String> {

    //@Query(nativeQuery = true, value = "select count(b.bank_code) FROM Bank b")
    @Query(value = "select count(b.bankCode) FROM Bank b")
    public Integer countBanksByBankCode();

//@Query(value = "select b from Bank b join fetch b.branches")
//@EntityGraph(value = "Bank.branches", type = EntityGraph.EntityGraphType.FETCH)
@EntityGraph(attributePaths = {"branches"},type = EntityGraph.EntityGraphType.FETCH)
//@Query(value = "select" +
//        "         b1_0.bank_code," +
//        "         b1_0.bank_name," +
//        "         b1_0.bank_opening_date," +
//        "         b1_0.bank_swift_code," +
//        "         b2_0.branch_code," +
//        "         b2_0.branch_name," +
//        "         b2_0.branch_opening_date " +
//        "     from" +
//        "         bank b1_0 " +
//        "     left join" +
//        "         branch b2_0 " +
//        "             on b1_0.bank_code=b2_0.bank_code", nativeQuery = true)
public List<Bank> findAll();



}
