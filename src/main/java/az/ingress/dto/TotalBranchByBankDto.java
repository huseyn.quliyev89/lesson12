package az.ingress.dto;


public interface TotalBranchByBankDto {
    String getBankName();
    Integer getTotalBranch();
}
