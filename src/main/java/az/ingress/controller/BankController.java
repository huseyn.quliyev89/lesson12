package az.ingress.controller;

import az.ingress.dto.BankDto;
import az.ingress.mapper.BankMapper;
import az.ingress.model.Bank;
import az.ingress.model.Branch;
import az.ingress.service.BankService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class BankController {
private final BankService bankService;
private final BankMapper bankMapper;

    public BankController(BankService bankService, BankMapper bankMapper) {
        this.bankService = bankService;
        this.bankMapper = bankMapper;
    }

    @GetMapping("/bank/{bankCode}")
    public BankDto getBankByBankCode(@PathVariable String bankCode) {
        return bankMapper.mapToBankDto(bankService.getBankByBankCode(bankCode)) ;
    }

    @GetMapping("/bank")
    public List<BankDto> getBanks() {
        //List<Bank> bankList = bankService.getBanks();
        List<Bank> bankList = bankService.getBanksAll();
        return bankMapper.mapToBankDtoList(bankList);
    }

    @GetMapping("/bank/count")
    public Integer countBanksByBankCode() {
        return bankService.countBanksByBankCode();
    }

    @PostMapping("/bank")
    public BankDto createBank(@RequestBody BankDto bankDto) {
        Bank bank =  bankMapper.mapToBank(bankDto);
        Bank savedBank = bankService.createBank(bank);
        return bankMapper.mapToBankDto(savedBank);
    }

    @PutMapping("/bank/{bankCode}")
    public BankDto updateBank(@PathVariable String bankCode, @RequestBody BankDto bankDto) {
        Bank bank =  bankMapper.mapToBank(bankDto);
        bank.setBankCode(bankCode);
        Bank savedBank = bankService.updateBank(bankCode, bank);
        return bankMapper.mapToBankDto(savedBank);
    }

    @DeleteMapping("/bank/{bankCode}")
    public void deleteBank(@PathVariable String bankCode) {
        bankService.deleteBranch(bankCode);
    }
}
