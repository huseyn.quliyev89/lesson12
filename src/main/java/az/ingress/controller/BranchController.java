package az.ingress.controller;

import az.ingress.dto.BranchDto;
import az.ingress.dto.TotalBranchByBankDto;
import az.ingress.mapper.BranchMapper;
import az.ingress.model.Bank;
import az.ingress.model.Branch;
import az.ingress.service.BankService;
import az.ingress.service.BranchService;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class BranchController {
    private final BranchService branchService;
    private final BankService bankService;
    private final BranchMapper branchMapper;

    public BranchController(BranchService branchService, BankService bankService, BranchMapper branchMapper) {
        this.branchService = branchService;
        this.bankService = bankService;
        this.branchMapper = branchMapper;
    }

    @GetMapping("/branch/{branchCode}")
    public BranchDto getBranchByBranchCode(@PathVariable String branchCode) {
        Branch branch = branchService.getBranchByBranchCode(branchCode);
        return branchMapper.mapToBranchDto(branch);
    }

    @GetMapping("/branch")
    public List<BranchDto> getAllBranch() {
       List<Branch> branchList = branchService.getBranches();

        return branchMapper.mapToBranchDtoList(
                branchList
        );
    }
    @GetMapping("/branch/totalbranchbybank")
    public List<TotalBranchByBankDto> totalBranchByBank() {
        return branchService.totalBranchByBank();
    }


    @PostMapping("/bank/{bankCode}/branch")
    public BranchDto createBranch(@PathVariable String bankCode, @RequestBody BranchDto branchDto) {
        Branch branch =  branchMapper.mapToBranch(branchDto);
        Bank bank = bankService.getBankByBankCode(bankCode);
        branch.setBank(bank);
        Branch savedBranch = branchService.createBranch(branch);
        return branchMapper.mapToBranchDto(savedBranch);
    }

    @PutMapping("/branch/{branchCode}")
    public BranchDto updateBranch(@PathVariable String branchCode, @RequestBody BranchDto branchDto) {
        Branch branch =  branchMapper.mapToBranch(branchDto);
        branch.setBranchCode(branchCode);
        Branch savedBranch = branchService.updateBranch(branchCode, branch);
        return branchMapper.mapToBranchDto(savedBranch);
    }

    @DeleteMapping("/branch/{branchCode}")
    public void deleteBranch(@PathVariable String branchCode) {
        branchService.deleteBranch(branchCode);
    }
}
