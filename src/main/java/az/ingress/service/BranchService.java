package az.ingress.service;

import az.ingress.dto.TotalBranchByBankDto;
import az.ingress.model.Branch;

import java.util.List;

public interface BranchService {
    Branch getBranchByBranchCode(String branchCode);

    List<Branch> getBranches();

    Branch createBranch(Branch branch);

    Branch updateBranch(String branchCode, Branch branch);

    void deleteBranch(String branchCode);

    List<TotalBranchByBankDto> totalBranchByBank();






}
