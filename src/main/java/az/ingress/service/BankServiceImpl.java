package az.ingress.service;

import az.ingress.model.Bank;
import az.ingress.model.Branch;
import az.ingress.repository.BankRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BankServiceImpl implements BankService{

    private final BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public Bank getBankByBankCode(String bankCode) {
        Optional<Bank> bank =   bankRepository.findById(bankCode);

        if (bank.isPresent()) {
            return bank.get();
        } else {
            throw new RuntimeException("Record not found with bankCode : " + bankCode);
        }
    }

    @Override
    public List<Bank> getBanks() {
        return bankRepository.findAll();
    }

    @Override
    public Bank createBank(Bank bank) {
        return bankRepository.save(bank);
    }

    @Override
    public Bank updateBank(String bankCode, Bank bank) {
        Optional<Bank> bankDb = bankRepository.findById(bank.getBankCode());

        if (bankDb.isPresent()) {
            Bank bankUpdate = bankDb.get();
            bankUpdate.setBankCode(bank.getBankCode());
            bankUpdate.setBankName(bank.getBankName());
            bankUpdate.setBankSwiftCode(bank.getBankSwiftCode());
            bankUpdate.setBankOpeningDate(bank.getBankOpeningDate());

            return bankRepository.save(bankUpdate);
        } else {
            throw new RuntimeException("Record not found with bankCode : " + bankCode);
        }
    }

    @Override
    public void deleteBranch(String branchCode) {
            bankRepository.deleteById(branchCode);
    }

    @Override
    public Integer countBanksByBankCode() {
        return bankRepository.countBanksByBankCode();
    }

    @Override
    public List<Bank> getBanksAll() {
        return bankRepository.findAll();
    }
}
