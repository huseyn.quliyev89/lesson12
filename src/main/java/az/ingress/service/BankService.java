package az.ingress.service;

import az.ingress.model.Bank;
import az.ingress.model.Branch;

import java.util.List;

public interface BankService {

    Bank getBankByBankCode(String bankCode);

    List<Bank> getBanks();

    List<Bank> getBanksAll();

    Integer countBanksByBankCode();

    Bank createBank(Bank bank);

    Bank updateBank(String bankCode, Bank bank);

    void deleteBranch(String branchCode);



}
